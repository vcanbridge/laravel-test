<?php

Route::get('/', 'SigninController@showLogin');
Route::post('/', 'SigninController@verifyLogin');
Route::get('signup', 'SignupController@showSignup');
Route::post('signup', array( 'before' => 'csrf', 'uses'=>'SignupController@createUser'));
Route::get('view', 'EditController@viewUser');
Route::post('edit', array( 'before' => 'csrf', 'uses'=>'EditController@editUser'));
Route::get('admin', array('before' => 'auth_admin', 'uses'=>'AdminController@listAllUser'));

Route::get('logout', function()
{   
    Session::flush();
    return Redirect::to('/');
});
