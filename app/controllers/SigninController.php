<?php
use Illuminate\Support\MessageBag;

class SigninController extends BaseController {

    public function showLogin() {

        return View::make('signin');
    }

    public function verifyLogin() {
        
        $user = array(
            'email' => Input::get('email'),
            'password' => Input::get('password')
        );

        if (Auth::attempt($user)) {
            return Redirect::to('view');
        }
        $errors = new MessageBag(['password' => ['Email and/or password invalid.']]); 
        
        return Redirect::back()->withErrors($errors)->withInput(Input::except('password'));
    }

}
