<?php

/**
 *
 * @author Chuan Li <chuan.li at vcanbridge.com>
 */
use Illuminate\Support\MessageBag;

class EditController extends BaseController {

    public function editUser() {
  
        $rules = array(
            'password' => 'same:password_confirm',
            'first_name' => 'required',
            'last_name' => 'required'
        );
        $validation = Validator::make(Input::all(), $rules);

        if ($validation->fails()) {
            $messages = $validation->messages();
            return Redirect::back()->withErrors($messages)->withInput(Input::except('password'));
        }

        $user = User::find(Auth::user()->id);
        if(  $user->password !== ''){ 
            $user->password = Hash::make(Input::get('password'));
        }
        $user->first_name = Input::get('first_name');
        $user->last_name = Input::get('last_name');
        $user->user_introduction = Input::get('user_introduction');
        if ($user->save()) {
            Auth::loginUsingId($user->id)->with('notify','Profile updated');
            return Redirect::to('view');
        }
        return Redirect::to('edit');
    }

    public function viewUser() {
        if (Auth::check()) {
            return View::make('edit')->with('user', Auth::user());
        } else {
           return Redirect::to('/')->with('login_error', 'Please login first.');
        }
    }

}
