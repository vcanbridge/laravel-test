<?php
/**
 *
 * @author Chuan Li <chuan.li at vcanbridge.com>
 */

class AdminController extends BaseController {
    
    public function listAllUser() {
        $users = User::all();
        return  View::make('admin')->with('users', $users);
    }
}
