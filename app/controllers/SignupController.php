<?php
/**
 *
 * @author Chuan Li <chuan.li at vcanbridge.com>
 */

class SignupController extends BaseController {
    
    public function showSignup() {
        return View::make('signup');
    }

    public function createUser() {

        $rules = array(
            'email' => 'required|email|unique:users',
            'password' => 'required|same:password_confirm',
            'first_name' => 'required',
            'last_name' => 'required'
        );
        $validation = Validator::make(Input::all(), $rules);
        
        if ($validation->fails()) {
            $messages = $validation->messages(); 
            return Redirect::back()->withErrors($messages)->withInput(Input::except('password'));
        }

        $user = new User;
        $user->email = Input::get('email');
        $user->password = Hash::make(Input::get('password'));
        $user->first_name = Input::get('first_name');
        $user->last_name = Input::get('last_name');
        if ($user->save()) {
            Auth::loginUsingId($user->id);
            return Redirect::to('view');
        }
    }

}
