@extends('layouts.kwfw')

@section('title')
Admin
@stop

@section('header')
<h2>Admin</h2>
@stop

@section('menu')
<ul>
    <li><a href="{{ URL::to('logout') }}"> Logout </a></li>
    <li><a href="{{ URL::to('view') }}"> Admin Profile</a></li>
</ul>
@stop

@section('main')
<table>
    <tr>
        <th>Email</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Introduction</th>
    </tr>
@foreach ($users as $user)
    <tr>
    <td>{{ $user->email }}</td>
    <td>{{ $user->first_name }}</td>
    <td>{{ $user->last_name }}</td>
    <td>{{ $user->user_intoduction }}</td>
    </tr>
@endforeach
</table>
@stop