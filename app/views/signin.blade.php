@extends('layouts.kwfw')

@section('title')
Signup
@stop

@section('header')
<h2>Welcome</h2>
@stop

@section('menu')
<ul>
    <li><a href="{{ URL::to('signup') }}"> Sign up now </a></li>
</ul>
@stop

@section('main')
{{ Form::open() }} 
        <div class="form_element">
        {{ Form::label('email', 'Email address: ') }}
        {{ Form::email('email', Input::old('email')) }}
        <br />
        </div>
        <div class="form_element">
        {{ Form::label('password', 'Password: ') }}
        {{ Form::password('password') }}
        </div>
        <div class="form_element">
        {{ Form::submit('SIGN IN') }} 
        </div>
{{ Form::close() }}

<div>  Don't have an account? <a href="{{ URL::to('signup') }}"> Sign up now </a> </div>

@stop

