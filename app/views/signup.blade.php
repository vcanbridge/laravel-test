@extends('layouts.kwfw')

@section('title')
Signup
@stop

@section('header')
<h2>Sign Up</h2>
@stop

@section('menu')
<ul>
    <li><a href="{{ URL::to('/') }}"> Sing In </a></li>
    <li><a href="{{ URL::to('admin') }}"> Admin </a></li>
</ul>
@stop

@section('main')
{{ Form::open() }} 
        <div class="form_element">
        {{ Form::label('email', 'Email address: ') }}
        {{ Form::email('email', Input::old('email')) }}
        <br />
        <span class="note">Your email address will be your login ID.</span>
        </div>
        <div class="form_element">
        {{ Form::label('first_name', 'First Name: ') }}
        {{ Form::text('first_name', Input::old('first_name')) }}
        </div>
        <div class="form_element">
        {{ Form::label('last_name', 'Last Name: ') }}
        {{ Form::text('last_name', Input::old('last_name')) }}
        </div>
        <div class="form_element">
        {{ Form::label('password', 'Password: ') }}
        {{ Form::password('password') }}
        </div>
        <div class="form_element">
        {{ Form::label('password_confirm','Retype Password: ') }}
        {{ Form::password('password_confirm') }}
        </div>
        <div class="form_element">
        {{ Form::submit('SIGN UP') }} 
        </div>
{{ Form::close() }}
@stop
