<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
        <title>Test | @yield('title')</title>
        <link href="{{ URL::asset('css/kwfw.css') }}" rel="stylesheet" media="screen,projection" >                
    </head>
    <body>
        <div id='outer'>
            <div id="header">@yield('header')</div>
            <div id="nav">@yield('menu')</div>
            <div id="main">@yield('main')</div> 
            <div id='footer'>
                @section('message')
                @foreach ($errors->all() as $error)
                <span class="error">
                    {{ $error }}
                </span>
                @endforeach
                @show
            </div>
        </div>
    </body>
</html>

