@extends('layouts.kwfw')

@section('title')
Edit Profile
@stop

@section('header')
<h2>Edit Profile</h2>
@stop

@section('menu')
<ul>
    <li><a href="{{ URL::to('logout') }}"> Logout </a></li>
    <li><a href="{{ URL::to('admin') }}"> Admin </a></li>
</ul>
@stop

@section('main')
{{ Form::open(array('url' => 'edit')) }} 
        <div class="form_element">
           Welcome! Your user id is: {{ $user->email }}
        </div>
        <div class="form_element">
        {{ Form::label('first_name', 'First Name: ') }}
        {{ Form::text('first_name', $user->first_name) }}
        </div>
        <div class="form_element">
        {{ Form::label('last_name', 'Last Name: ') }}
        {{ Form::text('last_name',  $user->last_name) }}
        </div>
        <div class="form_element">
        {{ Form::label('password', 'Password: ') }}
        {{ Form::password('password') }}
        </div>
        <div class="form_element">
        {{ Form::label('password_confirm','Retype Password: ') }}
        {{ Form::password('password_confirm') }}
        </div>
        <div class="form_element">
        {{ Form::label('user_introduction', 'About Yourself: ') }}
        {{ Form::textarea('user_introduction',  $user->user_introduction) }}
        </div>
        <div class="form_element">
        {{ Form::submit('Update') }} 
        </div>
{{ Form::close() }}
{{ $notify or ''}}
@stop